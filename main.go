package main

// https://www.digitalocean.com/community/tutorials/how-to-deploy-resilient-go-app-digitalocean-kubernetes

import (
	"fmt"
	"net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "My Awesome Go App")
}

func setupRoutes() {
	http.HandleFunc("/", homePage)
}

func main() {
	setupRoutes()
	fmt.Println("Go Web App Started on Port 8080")
	http.ListenAndServe(":8080", nil)
}
